Function Exists {
	param ($ip) 
	$return = Test-Connection $ip -count 4 -Quiet
	return $return
}

Function Decode {
    If ($args[0] -is [System.Array]) {
        [System.Text.Encoding]::ASCII.GetString($args[0])
    }
    Else {
        "Not Found"
    }
}

For ($i=0; $i -lt 100; $i++){

	$ip = "192.168.0."+$i

	if (Exists $ip) {

		$system = gwmi Win32_computersystem -Computer $ip
		$os = gwmi Win32_OperatingSystem -Computer $ip
		$bios = gwmi Win32_bios -Computer $ip
		
		# basic details
		$compName = [System.Net.Dns]::GetHostByAddress($ip).Hostname
		$compModel =$system.Model
		$username = $system.Username
		$serial = $bios.SerialNumber
		$caption = $os.Caption
		$architecture = $os.OSArchitecture
		$osbuild= $os.BuildNumber

		# RAM Capacity in GB, sums all the ram sticks available
		(gwmi win32_physicalmemory -namespace root\CIMV2 -Computer $ip).Capacity | Foreach {$ramGB += $_}
		$ramGB =$ramGb/1GB

		# Hard drive size and type
		$diskgb = [math]::Round((gwmi win32_logicaldisk -Computer $ip -Filter "DeviceID= 'C:'").Size/1GB)
		$mediatype = (gwmi MSFT_PhysicalDisk -namespace root\Microsoft\Windows\Storage -Computer $ip).mediatype
		if ($mediatype -eq 3 ){
			$diskType = 'HDD'
		}
		elseif ($mediatype -eq 4){
			$diskType = 'SSD'
		}

		# monitor details
		# from https://community.spiceworks.com/topic/658220-get-monitor-manufacturer-model-and-serial-number
		ForEach( $monitor in gwmi WmiMonitorID -namespace root\wmi -Computer $ip) {  
			echo "#"
			$Name = Decode $monitor.UserFriendlyName -notmatch 0
			$MonSerial = Decode $monitor.SerialNumberID -notmatch 0
			$Year = $monitor.YearOfManufacture
			$monitors += "$Name, $MonSerial, $Year; "
		}

		# video output types
		# from https://www.reddit.com/r/PowerShell/comments/6mp9px/getting_monitor_and_cable_information/
		ForEach ( $monitor in gwmi WmiMonitorConnectionParams -namespace root/wmi  -Computer $ip){
			$VideoOut = $monitor.VideoOutputTechnology 
			switch ( $VideoOut )
			{
				0 { $type = "VGA"    }
				4 { $type = "DVI"    }
				5 { $type = "HDMI"    }
				10 { $type = "DP-External"  }
				11 { $type = "DP-Embedded"   }
				2147483648{ $type = "Internal"   }
				default { $type = "Exotic, go check!"    }
		
			}
			$slots += "$type; "
		}

		# consolidate collected data into object and export to csv file
		New-Object -TypeName PSCustomObject -Property @{
			IP = $ip
			Name = $compName
			UserName = $username
			Serial = $serial
			Model = $compModel
			Architecture = $architecture
			OS = $caption
			Build = $osbuild
			RAM = $ramGB
			DiskSize = $diskGB
			DiskType = $diskType
			Monitors = $monitors
			VideoOuts = $slots 
		} |  Export-Csv -Path ./inventory.csv -NoTypeInformation -Append
	}
}
